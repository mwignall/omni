module PagesHelper
  # This could be nicer if we use the draper gem
  def screenshot_for(page)
    return nil unless page.screenshot.attachment

    image_tag(page.screenshot.variant(resize: '350').processed, width: '100%')
  end
end

class Page < ApplicationRecord
  validates :title, presence: true
  has_one_attached :screenshot
end

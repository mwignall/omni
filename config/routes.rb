Rails.application.routes.draw do
  resources :pages

  get 'welcome/index'
  root 'welcome#index'
end

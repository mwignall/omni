require 'test_helper'

class PagesHelperTest < ActionView::TestCase
  def setup
    @page = FactoryBot.create :page
  end

  test 'returns a screenshot' do
    screenshot_for(@page)
  end
end
